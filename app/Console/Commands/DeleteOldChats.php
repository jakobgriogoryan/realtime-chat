<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Chat;
use Carbon\Carbon;

class DeleteOldChats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:chats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Chat::where('created_at', '<', Carbon::now()->subDay())->delete();
    }
}
