<?php

namespace App\Http\Requests\Chat;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
         return [
             'message' => 'required|min:1|max:250',
             'receiver_id' => 'required',
         ];
     }

     public function params()
     {
         return [
             'message' => trim($this->message),
             'receiver_id' => $this->receiver_id,
             'sender_id' => auth()->id()
         ];
     }
}
