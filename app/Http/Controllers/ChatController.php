<?php

namespace App\Http\Controllers;

use App\Chat;
use App\User;
use App\Events\MessageSent;
use Illuminate\Http\Request;
use App\Http\Requests\Chat\StoreRequest;
use App\Jobs\SendEmailJob;

class ChatController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('chat');
    }

    public function fetchMessages()
    {
        return Chat::all();
    }

    public function fetchUsers()
    {
        $receivers = User::where('id', '!=', auth()->id())->get();
        $sender = auth()->user();

        return response()->json([
            'receivers' => $receivers,
            'sender' => $sender
        ]);
    }

    public function sendMessage(StoreRequest $request)
    {
        $message = Chat::create($request->params());
        $user = User::find($request->receiver_id);

        broadcast(new MessageSent($message));

        $details['email'] = $user->email;
        SendEmailJob::dispatch($details);

        return response()->json([
            'message' => $message
        ]);
    }
}
