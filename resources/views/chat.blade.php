@extends('layouts.app')

@section('content')
    <div class="container">
        <chat :sender='{{auth()->user()}}'></chat>
    </div>
@endsection
