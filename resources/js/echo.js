import Echo from "laravel-echo";
window.io = require('socket.io-client');

// Socket.io
window.Echo = new Echo({
    broadcaster: 'socket.io',
    host: window.location.origin + ':6001',
});
